package br.flpcostas.projeto.dominio.test;

import org.junit.Assert;
import org.junit.Test;

import br.flpcostas.projeto.dominio.servicos.ServicoDeAutenticacao;
import br.flpcostas.projeto.infraestrutura.servicos.ServicoDeAutenticacaoImpl;

public class AutenticacaoServicoTest {

	private ServicoDeAutenticacao autenticacaoServico;

	public AutenticacaoServicoTest() {
		autenticacaoServico = new ServicoDeAutenticacaoImpl();
	}

	@Test
	public void tenta_efetuar_login_test() {
		boolean aut = autenticacaoServico.efetuarLogin("flpcostas", "secret");
		System.out.println("tentaEfetuarLogin: " + aut);
		Assert.assertTrue(aut);
	}

}
