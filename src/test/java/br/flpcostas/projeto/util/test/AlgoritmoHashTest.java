package br.flpcostas.projeto.util.test;

import org.junit.Assert;
import org.junit.Test;

import br.flpcostas.util.AlgoritmoHash;

public class AlgoritmoHashTest {

	@Test
	public void tenta_converter_termo_test() {
		String termo = AlgoritmoHash.getHash("123");
		System.out.println("hash: " + termo);
		Assert.assertEquals(
				"A665A45920422F9D417E4867EFDC4FB8A04A1F3FFF1FA07E998E86F7F7A27AE3",
				termo);
	}

}
