package br.flpcostas.projeto.util.test;

import org.junit.Test;

import br.flpcostas.util.AlgoritmoBase64;

public class AlgoritmoBase64Test {

	@Test
	public void encode_test() {
		System.out.println("encode: " + AlgoritmoBase64.encode("filipe"));
	}

	@Test
	public void decode_test() {
		System.out.println("decode: " + AlgoritmoBase64.decode("ZmlsaXBl"));
	}
}
