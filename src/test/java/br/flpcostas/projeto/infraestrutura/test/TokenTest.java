package br.flpcostas.projeto.infraestrutura.test;

import org.junit.Assert;
import org.junit.Test;

import br.flpcostas.projeto.infraestrutura.persistencia.stubs.ContaMock;
import br.flpcostas.projeto.infraestrutura.util.Token;

public class TokenTest {

	ContaMock contaMock = new ContaMock();

	@Test
	public void tenta_gerar_token_test() {
		Token token = new Token();
		String tokenGerado = token.geraTokenAcesso(ContaMock.filtrarPorId(1L)
				.getLogin(), ContaMock.filtrarPorId(1L).getEmail());
		System.out.println("token gerado: " + tokenGerado);
		
		Assert.assertEquals("ZmxwY29zdGFzfGZscC5jb3N0YXNAZ21haWwuY29t",
				tokenGerado);

	}

	@Test
	public void tenta_decompor_token_test() {
		Token token = new Token();
		String[] array = token
				.decompoemTokenAcesso("fGZscGNvc3Rhc3xmbHBjb3N0YXNAZ21haWwuY29tfA==");
		System.out.println("login/email: " + array[1] + "/" + array[2]);
		
		Assert.assertEquals("flpcostas", array[1]);

	}
}
