package br.flpcostas.projeto.infraestrutura.persistencia.jpa.test;

import java.util.Collection;
import java.util.GregorianCalendar;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import br.flpcostas.projeto.dominio.modelo.entidade.Modelo;
import br.flpcostas.projeto.dominio.repositorio.Repositorio;
import br.flpcostas.projeto.infraestrutura.persistencia.jpa.ModeloRepositorioJpa;

/**
 * Classe base para testes de integracao com banco de dados
 * Testes de mapeamento e persistencia
 * 
 * Utilize a convencao a1, a2, a3, b1, b2, b3, etc para executa os testes
 * de maneira ordenada. 
 * O decorador @FixMethodOrder(MethodSorters.NAME_ASCENDING) e responsavel por permitir
 * que os metodos sejam ordenados pelo nome em ordem alfabetica.
 * 
 * @author filipe
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ModeloRepositorioTest {

	private Repositorio<Modelo> repositorio;

	public ModeloRepositorioTest() {
		repositorio = new ModeloRepositorioJpa();
	}

	@Test
	public void a1_tenta_salvar_modelo_test() {
		System.out.println("|>------- tenta_salvar_modelo_test -------- ");
		Modelo m = new Modelo();
		m.setCampoTexto("Texto qualquer...");
		m.setCampoData(new GregorianCalendar(1988, 8, 21));
		Modelo mp = repositorio.salvar(m);
		System.out.println("ID: " + mp.getId());
	}
	
	@Test
	public void a2_tenta_listar_modelo_test() {
		System.out.println("|>------- tenta_listar_modelo_test -------- ");
		Collection<Modelo> colecao = repositorio.listarTodos();
		colecao.stream().forEach(i -> System.out.println("ID: " + i.getId()));
	}
	
	@Test
	public void a3_test() {

	}

	@Test
	public void a4_test() {

	}

	@Test
	public void a5_test() {

	}
}
