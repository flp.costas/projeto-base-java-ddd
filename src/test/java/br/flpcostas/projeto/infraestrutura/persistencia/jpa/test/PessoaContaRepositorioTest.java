package br.flpcostas.projeto.infraestrutura.persistencia.jpa.test;

import java.util.Collection;
import java.util.GregorianCalendar;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import br.flpcostas.projeto.dominio.modelo.entidade.Conta;
import br.flpcostas.projeto.dominio.modelo.entidade.Pessoa;
import br.flpcostas.projeto.dominio.modelo.objetovalor.Perfil;
import br.flpcostas.projeto.dominio.modelo.objetovalor.Sexo;
import br.flpcostas.projeto.dominio.repositorio.ContaRepositorio;
import br.flpcostas.projeto.dominio.repositorio.PessoaRepositorio;
import br.flpcostas.projeto.infraestrutura.persistencia.jpa.ContaRepositorioJpa;
import br.flpcostas.projeto.infraestrutura.persistencia.jpa.PessoaRepositorioJpa;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PessoaContaRepositorioTest {

	private PessoaRepositorio pessoaRepositorio;
	private ContaRepositorio contaRepositorio;

	public PessoaContaRepositorioTest() {
		pessoaRepositorio = new PessoaRepositorioJpa();
		contaRepositorio = new ContaRepositorioJpa();
	}

	@Test
	public void a1_tenta_salvar_pessoa_test() {
		System.out.println("|>------- tenta_salvar_pessoa_test -------- ");
		Pessoa p = new Pessoa();
		p.setNome("Bruce");
		p.setSobrenome("Willis");
		p.setNascimento(new GregorianCalendar(1987, 7, 21));
		p.setSexo(Sexo.MASCULINO);
		Conta c = new Conta();
		c.setEmail("bwillis@nossomail.com");
		c.setLogin("willis");
		c.setPerfil(Perfil.USUARIO);
		c.setSenha("secret");
		p.setConta(c);
		Pessoa pp = pessoaRepositorio.salvar(p);
		System.out.println("ID: " + pp.getId());
	}

	@Test
	public void a2_tenta_buscar_pessoas_por_nome_test() {
		System.out.println("|>------- a2_tenta_buscar_pessoas_por_nome_test -------- ");
		Collection<Pessoa> colecao = pessoaRepositorio.porNome("Br");
		colecao.forEach(p -> System.out.println("ID: " + p.getId()));
	}
	
	@Test
	public void a3_tenta_buscar_por_login_e_senha_test() {
		System.out.println("|>------- a1_tenta_buscar_por_login_e_senha_test -------- ");
		Conta entidade = contaRepositorio.porLoginESenha("willis", "secret");
		System.out.println(entidade.getId());
	}

	
}
