package br.flpcostas.projeto.infraestrutura.persistencia.jpa.test;

import org.junit.Test;

import br.flpcostas.projeto.dominio.repositorio.SeedRepositorio;
import br.flpcostas.projeto.infraestrutura.persistencia.jpa.SeedRepositorioJpa;

public class SeedsPersistenciaTest {

	private SeedRepositorio repositorio;

	public SeedsPersistenciaTest() {
		repositorio = new SeedRepositorioJpa();
	}

	@Test
	public void tenta_criar_seeds_test() {
		repositorio.seed();
	}

	
}
