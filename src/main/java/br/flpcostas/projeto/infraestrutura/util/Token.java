package br.flpcostas.projeto.infraestrutura.util;

import br.flpcostas.util.AlgoritmoBase64;

public class Token {

	public String geraTokenAcesso(String login, String email) {
		return AlgoritmoBase64.encode(login + "|" + email);
	}

	public String[] decompoemTokenAcesso(String token) {
		String strParam = AlgoritmoBase64.decode(token);
		String[] param = strParam.split("\\|");
		return param;
	}

}
