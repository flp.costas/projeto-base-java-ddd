package br.flpcostas.projeto.infraestrutura.injecaodep.guice;

import br.flpcostas.projeto.aplicacao.AutenticacaoServico;
import br.flpcostas.projeto.aplicacao.ContaServico;
import br.flpcostas.projeto.aplicacao.impl.AutenticacaoServicoImpl;
import br.flpcostas.projeto.aplicacao.impl.ContaServicoImpl;
import br.flpcostas.projeto.dominio.repositorio.ContaRepositorio;
import br.flpcostas.projeto.dominio.repositorio.PessoaRepositorio;
import br.flpcostas.projeto.dominio.repositorio.SeedRepositorio;
import br.flpcostas.projeto.dominio.servicos.ServicoDeAutenticacao;
import br.flpcostas.projeto.infraestrutura.persistencia.jpa.ContaRepositorioJpa;
import br.flpcostas.projeto.infraestrutura.persistencia.jpa.PessoaRepositorioJpa;
import br.flpcostas.projeto.infraestrutura.persistencia.jpa.SeedRepositorioJpa;
import br.flpcostas.projeto.infraestrutura.servicos.ServicoDeAutenticacaoImpl;

import com.google.inject.AbstractModule;

public class GuiceModuloRegistro extends AbstractModule {

	@Override
	protected void configure() {
		injetarServicosDominio();
		injetaRepositorios();
		injetaServicosAplicacao();
	}

	protected void injetarServicosDominio() {
		bind(ServicoDeAutenticacao.class).to(ServicoDeAutenticacaoImpl.class);
	}
	
	protected void injetaRepositorios() {
		bind(SeedRepositorio.class).to(SeedRepositorioJpa.class);
		bind(ContaRepositorio.class).to(ContaRepositorioJpa.class);
		bind(PessoaRepositorio.class).to(PessoaRepositorioJpa.class);
	}

	protected void injetaServicosAplicacao() {
		bind(AutenticacaoServico.class).to(AutenticacaoServicoImpl.class);
		bind(ContaServico.class).to(ContaServicoImpl.class);
	}

}