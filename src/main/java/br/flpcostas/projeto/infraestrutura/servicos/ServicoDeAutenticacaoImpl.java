package br.flpcostas.projeto.infraestrutura.servicos;

import br.flpcostas.projeto.dominio.modelo.entidade.Conta;
import br.flpcostas.projeto.dominio.repositorio.ContaRepositorio;
import br.flpcostas.projeto.dominio.servicos.ServicoDeAutenticacao;
import br.flpcostas.projeto.infraestrutura.injecaodep.guice.GuiceModuloRegistro;
import br.flpcostas.projeto.infraestrutura.util.Token;
import br.flpcostas.util.AlgoritmoHash;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class ServicoDeAutenticacaoImpl implements ServicoDeAutenticacao {

	private Conta conta;
	private String tokenAcesso;
	private ContaRepositorio repositorio;

	@Override
	public String getTokenAcesso() {
		return tokenAcesso;
	}

	private void setTokenAcesso(String tokenAcesso) {
		this.tokenAcesso = tokenAcesso;
	}

	/**
	 * TODO: Necessario passar injecoes pelo constrututor. Verificar a
	 * implementacao com google guice.
	 * 
	 */
	public ServicoDeAutenticacaoImpl() {
		Injector injector = Guice.createInjector(new GuiceModuloRegistro());
		this.repositorio = injector.getInstance(ContaRepositorio.class);
	}

	@Override
	public boolean efetuarLogin(String lg, String sn) {
		conta = buscarPorLoginESenha(lg, sn);
		if (conta == null)
			return false;
	
		if (conta.getLogin().equals(lg) && conta.getSenha().equals(AlgoritmoHash.getHash(sn))) {
			Token token = new Token();
			setTokenAcesso(token.geraTokenAcesso(conta.getLogin(),
					conta.getEmail()));
			return true;
		}
		
		return false;
	}

	private Conta buscarPorLoginESenha(String lg, String sn) {
		return repositorio.porLoginESenha(lg, sn);
	}

}
