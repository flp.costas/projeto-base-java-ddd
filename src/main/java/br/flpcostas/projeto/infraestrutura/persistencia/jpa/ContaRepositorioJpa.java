package br.flpcostas.projeto.infraestrutura.persistencia.jpa;

import javax.persistence.Query;

import br.flpcostas.projeto.dominio.modelo.entidade.Conta;
import br.flpcostas.projeto.dominio.repositorio.ContaRepositorio;
import br.flpcostas.projeto.infraestrutura.persistencia.jpa.datamodel.ContaDataModel;
import br.flpcostas.util.AlgoritmoHash;

public class ContaRepositorioJpa extends EntityManagerRepositorio implements
		ContaRepositorio {

	@Override
	public Conta porLoginESenha(String lg, String sn) {
		String jpql = "select c from ContaDataModel c where c.login = :login and c.senha = :senha";
		Query query = getEntityManager()
				.createQuery(jpql, ContaDataModel.class);
		query.setParameter("login", lg).setParameter("senha",
				AlgoritmoHash.getHash(sn));
		ContaDataModel datamodel = (ContaDataModel) query.getSingleResult();

		return fromDataModel(datamodel);
	}

	public ContaDataModel toDataModel(Conta entidade) {
		return new ContaDataModel(entidade.getId(), entidade.getLogin(),
				entidade.getEmail(), entidade.getSenha(), entidade.getPerfil());
	}

	public Conta fromDataModel(ContaDataModel datamodel) {
		Conta entidade = new Conta(datamodel.getLogin(), datamodel.getEmail(),
				datamodel.getSenha(), datamodel.getPerfil());
		entidade.setId(datamodel.getId());
		return entidade;

	}

}
