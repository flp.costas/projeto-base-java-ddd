package br.flpcostas.projeto.infraestrutura.persistencia.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Classe responsavel por fabricar EntityManager da JPA
 * 
 * @author Filipe
 *
 */
public class JpaUtil {

	private static final String PERSISTENCE_UNIT = "heroku_postgres_validacao";
	private static EntityManagerFactory factory;

	public static EntityManager getEntityManager() {
		return getEntityManager(PERSISTENCE_UNIT);
	}

	public static EntityManager getEntityManager(String persistenceUnity) {
		factory = Persistence.createEntityManagerFactory(persistenceUnity);
		return factory.createEntityManager();
	}

	public static void fechaManagerFactory() {
		factory.close();
	}

	public static void fechaManager(EntityManager manager) {
		manager.close();
	}

}
