package br.flpcostas.projeto.infraestrutura.persistencia.stubs;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;

import br.flpcostas.projeto.dominio.modelo.entidade.Conta;
import br.flpcostas.projeto.dominio.modelo.entidade.Pessoa;
import br.flpcostas.projeto.dominio.modelo.objetovalor.Sexo;

public class PessoaMock {

	public static Pessoa filtrarPorLogin(String login) {
		List<Pessoa> lista = PessoaMock.lista();
		List<Pessoa> filtro = lista.stream()
				.filter(p -> p.getConta().getLogin().equals(login))
				.collect(Collectors.toList());

		if (filtro.size() > 0)
			return filtro.get(0);

		return null;
	}

	public static List<Pessoa> filtrarPorNome(String nome) {
		List<Pessoa> lista = PessoaMock.lista();
		List<Pessoa> filtro = lista.stream()
				.filter(p -> p.getNome().equals(nome))
				.collect(Collectors.toList());

		if (filtro.size() > 0)
			return filtro;

		return null;
	}

	public static Pessoa filtrarPorEmail(String email) {
		List<Pessoa> lista = PessoaMock.lista();
		List<Pessoa> filtro = lista.stream()
				.filter(p -> p.getConta().getEmail().equals(email))
				.collect(Collectors.toList());

		if (filtro.size() > 0)
			return filtro.get(0);

		return null;
	}

	public static List<Pessoa> lista() {
		List<Conta> conta = ContaMock.lista();

		Pessoa p1 = new Pessoa();
		p1.setId(1);
		p1.setNome("Filipe");
		p1.setSobrenome("Costa");
		p1.setSexo(Sexo.MASCULINO);
		p1.setNascimento(new GregorianCalendar(1984, 2, 23));
		p1.setConta(conta.get(0));

		Pessoa p2 = new Pessoa();
		p2.setId(2);
		p2.setNome("Jessica");
		p2.setSobrenome("Alba");
		p2.setSexo(Sexo.FEMININO);
		p2.setNascimento(new GregorianCalendar(1986, 1, 3));
		p2.setConta(conta.get(1));

		Pessoa p3 = new Pessoa();
		p3.setId(3);
		p3.setNome("Angelina");
		p3.setSobrenome("Jolie");
		p3.setSexo(Sexo.FEMININO);
		p3.setNascimento(new GregorianCalendar(1986, 2, 21));
		p3.setConta(conta.get(2));

		List<Pessoa> lista = new ArrayList<Pessoa>();
		lista.add(p1);
		lista.add(p2);
		lista.add(p3);

		return lista;
	}
}
