package br.flpcostas.projeto.infraestrutura.persistencia.stubs;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import br.flpcostas.projeto.dominio.modelo.entidade.Conta;
import br.flpcostas.projeto.dominio.modelo.objetovalor.Perfil;
import br.flpcostas.util.AlgoritmoHash;

public class ContaMock {

	public static Conta filtrarPorId(long id) {
		List<Conta> lista = ContaMock.lista();
		List<Conta> filtro = lista.stream().filter(c -> c.getId() == id)
				.collect(Collectors.toList());

		if(filtro.size() > 0)
			return filtro.get(0);
		
		return null;
	}

	public static Conta filtrarPorLoginSenha(String login, String senha) {
		List<Conta> lista = ContaMock.lista();
		List<Conta> filtro = lista.stream().filter(c -> c.getLogin().equals(login))
				.collect(Collectors.toList());

		if (filtro.size() > 0)
			return filtro.get(0);

		return null;
	}

	public static List<Conta> lista() {
		Conta c1 = new Conta();
		c1.setId(1);
		c1.setLogin("flpcostas");
		c1.setEmail("flp.costas@gmail.com");
		c1.setSenha(AlgoritmoHash.getHash("1234"));
		c1.setPerfil(Perfil.USUARIO);
		Conta c2 = new Conta();
		c2.setId(2);
		c2.setLogin("jessica");
		c2.setEmail("jessicaalba@gmail.com");
		c2.setSenha(AlgoritmoHash.getHash("tatu"));
		c2.setPerfil(Perfil.USUARIO);
		Conta c3 = new Conta();
		c3.setId(3);
		c3.setLogin("angelinajolie");
		c3.setEmail("angelina.jolie@gmail.com");
		c3.setSenha(AlgoritmoHash.getHash("secret"));
		c3.setPerfil(Perfil.ADMINISTRADOR);

		List<Conta> lista = new ArrayList<Conta>();
		lista.add(c1);
		lista.add(c2);
		lista.add(c3);

		return lista;
	}

}
