package br.flpcostas.projeto.infraestrutura.persistencia.stubs;

import br.flpcostas.projeto.dominio.modelo.entidade.Conta;
import br.flpcostas.projeto.dominio.repositorio.ContaRepositorio;

public class ContaRepositorioStubs implements ContaRepositorio {

	@Override
	public Conta porLoginESenha(String lg, String sn) {
		Conta c = ContaMock.filtrarPorLoginSenha(lg, sn);
		return c;
	}

}
