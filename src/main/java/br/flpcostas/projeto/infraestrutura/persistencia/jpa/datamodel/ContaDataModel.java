package br.flpcostas.projeto.infraestrutura.persistencia.jpa.datamodel;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.flpcostas.projeto.dominio.modelo.objetovalor.Perfil;

@Entity
@Table(name = "\"CONTA\"")
public class ContaDataModel implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "conta_seq", sequenceName = "conta_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "conta_seq")
	@Column(name = "\"ID\"", nullable = false)
	private long id;

	@Column(name = "\"LOGIN\"")
	private String login;

	@Column(name = "\"EMAIL\"")
	private String email;

	@Column(name = "\"SENHA\"")
	private String senha;

	@Column(name = "\"PERFIL\"")
	private Perfil perfil;

	public ContaDataModel() {

	}

	public ContaDataModel(long id, String login, String email, String senha,
			Perfil perfil) {
		this.id = id;
		this.login = login;
		this.email = email;
		this.senha = senha;
		this.perfil = perfil;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}
}
