package br.flpcostas.projeto.infraestrutura.persistencia.jpa.datamodel;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.flpcostas.projeto.dominio.modelo.objetovalor.Sexo;

@Entity
@Table(name = "\"PESSOA\"")
public class PessoaDataModel implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "pessoa_seq", sequenceName = "pessoa_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pessoa_seq")
	@Column(name = "\"ID\"", nullable = false)
	private long id;

	@Column(name = "\"NOME\"")
	private String nome;

	@Column(name = "\"SOBRENOME\"")
	private String sobrenome;

	@Column(name = "\"NASCIMENTO\"")
	@Temporal(value = TemporalType.DATE)
	private Calendar nascimento;

	@Column(name = "\"SEXO\"")
	private Sexo sexo;

	@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "\"CONTA_ID\"")
	private ContaDataModel conta;

	public PessoaDataModel() {

	}

	public PessoaDataModel(long id, String nome, String sobrenome,
			Calendar nascimento, Sexo sexo, ContaDataModel conta) {
		this.id = id;
		this.nome = nome;
		this.sobrenome = sobrenome;
		this.nascimento = nascimento;
		this.sexo = sexo;
		this.conta = conta;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public Calendar getNascimento() {
		return nascimento;
	}

	public void setNascimento(Calendar nascimento) {
		this.nascimento = nascimento;
	}

	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	public ContaDataModel getConta() {
		return conta;
	}

	public void setConta(ContaDataModel conta) {
		this.conta = conta;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
