package br.flpcostas.projeto.infraestrutura.persistencia.jpa;

import java.util.Collection;
import java.util.stream.Collectors;

import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import br.flpcostas.projeto.dominio.modelo.entidade.Conta;
import br.flpcostas.projeto.dominio.modelo.entidade.Pessoa;
import br.flpcostas.projeto.dominio.repositorio.PessoaRepositorio;
import br.flpcostas.projeto.infraestrutura.persistencia.jpa.datamodel.ContaDataModel;
import br.flpcostas.projeto.infraestrutura.persistencia.jpa.datamodel.PessoaDataModel;
import br.flpcostas.util.AlgoritmoHash;

public class PessoaRepositorioJpa extends EntityManagerRepositorio implements
		PessoaRepositorio {

	@Override
	public Collection<Pessoa> porNome(String nome) {
		String jpql = "select p from PessoaDataModel p where p.nome like concat('%', :nome, '%')";
		Query query = getEntityManager().createQuery(jpql,
				PessoaDataModel.class);
		query.setParameter("nome", nome);
		return getCollection(query);
	}

	@Override
	public Pessoa porEmail(String email) {
		String jpql = "select p from PessoaDataModel p where p.conta.email = :email";
		Query query = getEntityManager().createQuery(jpql,
				PessoaDataModel.class);
		query.setParameter("email", email);
		PessoaDataModel datamodel = (PessoaDataModel) query.getSingleResult();
		return fromDataModel(datamodel);
	}

	@Override
	public Pessoa porLogin(String login) {
		String jpql = "select p from PessoaDataModel p where p.conta.login = :login";
		Query query = getEntityManager().createQuery(jpql,
				PessoaDataModel.class);
		query.setParameter("login", login);
		PessoaDataModel datamodel = (PessoaDataModel) query.getSingleResult();
		return fromDataModel(datamodel);
	}

	@Override
	public Pessoa salvar(Pessoa entidade) {
		PessoaDataModel datamodel = toDataModel(entidade);

		EntityTransaction transaction = getEntityManager().getTransaction();
		transaction.begin();

		if (datamodel.getId() == 0L)
			getEntityManager().persist(datamodel);
		else
			getEntityManager().merge(datamodel);

		transaction.commit();

		return fromDataModel(datamodel);
	}

	/**
	 * Converte a colecao do tipo DataModel em colecao do tipo Entidade de dominio
	 * @param query Query
	 * @return Collection<Pessoa>
	 */
	@SuppressWarnings("unchecked")
	private Collection<Pessoa> getCollection(Query query) {
		Collection<PessoaDataModel> colecaoDatamodel = (Collection<PessoaDataModel>) query
				.getResultList();
		Collection<Pessoa> colecao = colecaoDatamodel.stream()
				.map(e -> fromDataModel(e)).collect(Collectors.toList());
		return colecao;
	}

	public PessoaDataModel toDataModel(Pessoa entidade) {
		PessoaDataModel datamodel = new PessoaDataModel();
		ContaDataModel datamodel2 = new ContaDataModel();
		datamodel2.setId(entidade.getConta().getId());
		datamodel2.setEmail(entidade.getConta().getEmail());
		datamodel2.setLogin(entidade.getConta().getLogin());
		datamodel2.setPerfil(entidade.getConta().getPerfil());
		datamodel2.setSenha(AlgoritmoHash.getHash(entidade.getConta().getSenha()));
		datamodel.setId(entidade.getId());
		datamodel.setNome(entidade.getNome());
		datamodel.setSobrenome(entidade.getSobrenome());
		datamodel.setNascimento(entidade.getNascimento());
		datamodel.setSexo(entidade.getSexo());
		datamodel.setConta(datamodel2);

		return datamodel;
	}

	public Pessoa fromDataModel(PessoaDataModel dm) {
		Pessoa entidade = new Pessoa();
		Conta entidade2 = new Conta();
		entidade2.setId(dm.getConta().getId());
		entidade2.setEmail(dm.getConta().getEmail());
		entidade2.setLogin(dm.getConta().getLogin());
		entidade2.setPerfil(dm.getConta().getPerfil());
		entidade2.setSenha(dm.getConta().getSenha());
		entidade.setId(dm.getId());
		entidade.setNome(dm.getNome());
		entidade.setSobrenome(dm.getSobrenome());
		entidade.setNascimento(dm.getNascimento());
		entidade.setSexo(dm.getSexo());
		entidade.setConta(entidade2);

		return entidade;
	}

}
