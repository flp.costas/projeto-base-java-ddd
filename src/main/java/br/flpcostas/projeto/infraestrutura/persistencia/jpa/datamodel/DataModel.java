package br.flpcostas.projeto.infraestrutura.persistencia.jpa.datamodel;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "\"MODELO\"")
public class DataModel implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "modelo_seq", sequenceName = "modelo_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "modelo_seq")
	@Column(name = "\"ID\"", nullable = false)
	private long id;

	@Column(name = "\"TEXTO\"")
	private String campoTexto;

	@Column(name = "\"DATA\"")
	@Temporal(value = TemporalType.DATE)
	private Calendar campoData;

	public DataModel() {
	}
	
	public DataModel(long id, String campoTexto, Calendar campoData) {
		this.id = id;
		this.campoTexto = campoTexto;
		this.campoData = campoData;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCampoTexto() {
		return campoTexto;
	}

	public void setCampoTexto(String campoTexto) {
		this.campoTexto = campoTexto;
	}

	public Calendar getCampoData() {
		return campoData;
	}

	public void setCampoData(Calendar campoData) {
		this.campoData = campoData;
	}

}
