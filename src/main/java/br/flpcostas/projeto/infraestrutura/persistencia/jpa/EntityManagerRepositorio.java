package br.flpcostas.projeto.infraestrutura.persistencia.jpa;

import javax.persistence.EntityManager;

public abstract class EntityManagerRepositorio {

	private static final EntityManager entityManager = JpaUtil.getEntityManager();
	
	public EntityManager getEntityManager() {
			return entityManager;
		
	}

}
