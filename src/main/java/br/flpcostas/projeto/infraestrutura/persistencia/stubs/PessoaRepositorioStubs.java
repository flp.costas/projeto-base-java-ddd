package br.flpcostas.projeto.infraestrutura.persistencia.stubs;

import java.util.Collection;
import java.util.List;

import br.flpcostas.projeto.dominio.modelo.entidade.Conta;
import br.flpcostas.projeto.dominio.modelo.entidade.Pessoa;
import br.flpcostas.projeto.dominio.repositorio.PessoaRepositorio;
import br.flpcostas.util.AlgoritmoHash;

public class PessoaRepositorioStubs implements PessoaRepositorio {

	@Override
	public Pessoa porLogin(String login) {
		Pessoa p = PessoaMock.filtrarPorLogin(login);
		return p;
	}

	@Override
	public Collection<Pessoa> porNome(String nome) {
		Collection<Pessoa> colecao = PessoaMock.filtrarPorNome(nome);
		return colecao;
	}

	@Override
	public Pessoa porEmail(String email) {
		Pessoa p = PessoaMock.filtrarPorEmail(email);
		return p;
	}

	@Override
	public Pessoa salvar(Pessoa pessoa) {
		Pessoa p = new Pessoa(pessoa.getNome(), pessoa.getSobrenome(),
				pessoa.getNascimento(), pessoa.getSexo(), new Conta(pessoa
						.getConta().getLogin(), pessoa.getConta().getEmail(),
						AlgoritmoHash.getHash(pessoa.getConta().getSenha()),
						pessoa.getConta().getPerfil()));
		List<Pessoa> lista = PessoaMock.lista();
		lista.add(p);
		return p;
	}

}
