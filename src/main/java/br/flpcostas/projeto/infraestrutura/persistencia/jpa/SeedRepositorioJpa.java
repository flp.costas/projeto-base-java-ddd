package br.flpcostas.projeto.infraestrutura.persistencia.jpa;

import java.util.GregorianCalendar;

import javax.persistence.EntityTransaction;

import br.flpcostas.projeto.dominio.modelo.objetovalor.Perfil;
import br.flpcostas.projeto.dominio.modelo.objetovalor.Sexo;
import br.flpcostas.projeto.dominio.repositorio.SeedRepositorio;
import br.flpcostas.projeto.infraestrutura.persistencia.jpa.datamodel.ContaDataModel;
import br.flpcostas.projeto.infraestrutura.persistencia.jpa.datamodel.DataModel;
import br.flpcostas.projeto.infraestrutura.persistencia.jpa.datamodel.PessoaDataModel;
import br.flpcostas.util.AlgoritmoHash;

public class SeedRepositorioJpa extends EntityManagerRepositorio implements
		SeedRepositorio {

	@Override
	public void seed() {

		criar(new DataModel(0, "Modelo principal", new GregorianCalendar(2012,
				2, 22)));

		criar(new PessoaDataModel(0, "Filipe", "Costa Silva",
				new GregorianCalendar(1984, 2, 23), Sexo.MASCULINO,
				new ContaDataModel(0, "flpcostas", "flp.costas@gmail.com",
						AlgoritmoHash.getHash("secret"), Perfil.USUARIO)));
		
		criar(new PessoaDataModel(0, "Juliana", "da Silva Costa",
				new GregorianCalendar(1977, 7, 13), Sexo.FEMININO,
				new ContaDataModel(0, "julianascosta", "julianascosta@gmail.com",
						AlgoritmoHash.getHash("mudai"), Perfil.USUARIO)));
		
		criar(new PessoaDataModel(0, "Julia", "Robert",
				new GregorianCalendar(1969, 1, 20), Sexo.FEMININO,
				new ContaDataModel(0, "julia", "juliarbt@gmail.com",
						AlgoritmoHash.getHash("pass"), Perfil.USUARIO)));

	}

	private void criar(Object datamodel) {
		EntityTransaction transaction = getEntityManager().getTransaction();
		transaction.begin();
		getEntityManager().persist(datamodel);
		transaction.commit();
	}

}
