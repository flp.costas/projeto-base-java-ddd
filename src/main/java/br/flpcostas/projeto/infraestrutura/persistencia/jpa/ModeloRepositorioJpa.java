package br.flpcostas.projeto.infraestrutura.persistencia.jpa;

import java.util.Collection;
import java.util.stream.Collectors;

import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import br.flpcostas.projeto.dominio.modelo.entidade.Modelo;
import br.flpcostas.projeto.dominio.repositorio.Repositorio;
import br.flpcostas.projeto.infraestrutura.persistencia.jpa.datamodel.DataModel;

/**
 * Classe referencia de implementacao do Repositorio Implementar a interface de
 * repositorio generico quando for necessario criar o crud padrao para entidade
 * Caso a entidade necessite de metodos especificos, criar interfaces de
 * repositorio especificas
 * 
 * @author filipe
 *
 */
public class ModeloRepositorioJpa extends EntityManagerRepositorio implements
		Repositorio<Modelo> {

	@Override
	public Modelo porId(long id) {
		DataModel query = null;
		query = getEntityManager().find(DataModel.class, id);
		return fromDataModel(query);
	}

	@Override
	public Collection<Modelo> listarTodos() {
		String jpql = "select m from DataModel m";
		Query query = getEntityManager().createQuery(jpql, DataModel.class);
		return getCollection(query);
	}

	@Override
	public Modelo salvar(Modelo entidade) {
		DataModel datamodel = toDataModel(entidade);
		EntityTransaction transaction = getEntityManager().getTransaction();
		transaction.begin();
		if (datamodel.getId() == 0L) {
			getEntityManager().persist(datamodel);
		} else {
			datamodel = getEntityManager().merge(datamodel);
		}
		transaction.commit();

		return fromDataModel(datamodel);
	}

	/**
	 * Converter uma colecao de datamodel em colecao de entidade. Esse metodo
	 * utiliza a expressao lambda do Java 8, metodo stream e map
	 * 
	 * @param Query
	 * @return colecao de entidade
	 */
	@SuppressWarnings("unchecked")
	private Collection<Modelo> getCollection(Query query) {
		Collection<DataModel> colecaoDatamodel = query.getResultList();
		Collection<Modelo> colecao = colecaoDatamodel.stream()
				.map(e -> fromDataModel(e)).collect(Collectors.toList());
		return colecao;
	}

	/**
	 * Converte Entidade em DataModel
	 * 
	 * @param entidade
	 * @return datamodel
	 */
	public DataModel toDataModel(Modelo entidade) {
		DataModel datamodel = new DataModel();
		datamodel.setId(entidade.getId());
		datamodel.setCampoTexto(entidade.getCampoTexto());
		datamodel.setCampoData(entidade.getCampoData());
		return datamodel;
	}

	/**
	 * Converter DataModel em Entidade
	 * 
	 * @param datamodel
	 * @return entidade
	 */
	public Modelo fromDataModel(DataModel datamodel) {
		Modelo entidade = new Modelo();
		entidade.setId(datamodel.getId());
		entidade.setCampoTexto(datamodel.getCampoTexto());
		entidade.setCampoData(datamodel.getCampoData());
		return entidade;
	}

}
