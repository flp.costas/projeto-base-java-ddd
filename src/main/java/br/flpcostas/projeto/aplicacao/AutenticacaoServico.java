package br.flpcostas.projeto.aplicacao;

import br.flpcostas.projeto.dominio.modelo.entidade.Pessoa;

public interface AutenticacaoServico {

	Pessoa efetuarLogin(String login, String senha);
	String getTokenAcesso();
}
