package br.flpcostas.projeto.aplicacao;

import br.flpcostas.projeto.dominio.modelo.entidade.Pessoa;

public interface ContaServico {

	Pessoa criar(Pessoa conta);
}
