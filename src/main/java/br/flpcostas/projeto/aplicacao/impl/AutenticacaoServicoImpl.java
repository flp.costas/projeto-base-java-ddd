package br.flpcostas.projeto.aplicacao.impl;

import br.flpcostas.projeto.aplicacao.AutenticacaoServico;
import br.flpcostas.projeto.dominio.modelo.entidade.Pessoa;
import br.flpcostas.projeto.dominio.repositorio.PessoaRepositorio;
import br.flpcostas.projeto.dominio.servicos.ServicoDeAutenticacao;
import br.flpcostas.projeto.infraestrutura.injecaodep.guice.GuiceModuloRegistro;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class AutenticacaoServicoImpl implements AutenticacaoServico {

	private PessoaRepositorio pessoaRepositorio;
	private ServicoDeAutenticacao servicoAutenticacao;

	public AutenticacaoServicoImpl() {
		Injector injector = Guice.createInjector(new GuiceModuloRegistro());
		pessoaRepositorio = injector.getInstance(PessoaRepositorio.class);
		servicoAutenticacao = injector.getInstance(ServicoDeAutenticacao.class);
	}

	@Override
	public Pessoa efetuarLogin(String login, String senha) {
		if (servicoAutenticacao.efetuarLogin(login, senha)) {
			return pessoaRepositorio.porLogin(login);
		}
		return null;
	}

	@Override
	public String getTokenAcesso() {
		return servicoAutenticacao.getTokenAcesso();
	}

}
