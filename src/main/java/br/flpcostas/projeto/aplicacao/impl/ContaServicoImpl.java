package br.flpcostas.projeto.aplicacao.impl;

import com.google.inject.Guice;
import com.google.inject.Injector;

import br.flpcostas.projeto.aplicacao.ContaServico;
import br.flpcostas.projeto.dominio.modelo.entidade.Pessoa;
import br.flpcostas.projeto.dominio.repositorio.PessoaRepositorio;
import br.flpcostas.projeto.infraestrutura.injecaodep.guice.GuiceModuloRegistro;

public class ContaServicoImpl implements ContaServico {

	private PessoaRepositorio repositorio;
	
	public ContaServicoImpl() {
		Injector injector = Guice.createInjector(new GuiceModuloRegistro());
		this.repositorio = injector.getInstance(PessoaRepositorio.class);
	}

	@Override
	public Pessoa criar(Pessoa conta) {
		return repositorio.salvar(conta);
	}

}
