package br.flpcostas.projeto.dominio.servicos;

public interface ServicoDeAutenticacao {
	
	String getTokenAcesso();
	boolean efetuarLogin(String lg, String sn);

}
