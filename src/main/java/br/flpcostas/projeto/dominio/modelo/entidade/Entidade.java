package br.flpcostas.projeto.dominio.modelo.entidade;

public interface Entidade {

	void verificaNuloOuVazio(Object objeto, String mensagem);
}
