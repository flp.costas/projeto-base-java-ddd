package br.flpcostas.projeto.dominio.modelo.entidade;

import br.flpcostas.projeto.dominio.modelo.objetovalor.Perfil;

public class Conta extends EntidadeBase {

	private long id;
	private String login;
	private String email;
	private String senha;
	private Perfil perfil;

	public Conta() {

	}

	public Conta(String login, String email, String senha, Perfil perfil) {
		this.login = login;
		this.email = email;
		this.senha = senha;
		this.perfil = perfil;

		validar(login);
	}

	public Conta(long id, String login, String email, String senha,
			Perfil perfil) {
		this.id = id;
		this.login = login;
		this.email = email;
		this.senha = senha;
		this.perfil = perfil;

		validar(login);
	}

	private void validar(String login) {
		verificaNuloOuVazio(login, "Login não pode ser nulo.");
		verificaNuloOuVazio(login, "E-mail não pode ser nulo.");
		verificaNuloOuVazio(login, "Senhya não pode ser nula.");
		verificaNuloOuVazio(login, "Deve existir um perfil.");
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

}
