package br.flpcostas.projeto.dominio.modelo.objetovalor;

public enum Perfil {

	ADMINISTRADOR(1), USUARIO(2);
	public int valor;

	Perfil(int valor) {
		this.valor = valor;
	}
}
