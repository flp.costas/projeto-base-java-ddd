package br.flpcostas.projeto.dominio.modelo.entidade;

public class EntidadeBase implements Entidade {

	@Override
	public void verificaNuloOuVazio(Object objeto, String mensagem) {
		if (objeto == null || objeto.equals("")) {
			throw new IllegalArgumentException(mensagem);
		}
	}

}
