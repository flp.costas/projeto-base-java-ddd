package br.flpcostas.projeto.dominio.modelo.entidade;

import java.util.Calendar;

import br.flpcostas.projeto.dominio.modelo.objetovalor.Sexo;

public class Pessoa extends EntidadeBase {

	private long id;
	private String nome;
	private String sobrenome;
	private Calendar nascimento;
	private Sexo sexo;
	private Conta conta;

	public Pessoa() {
	}

	public Pessoa(String nome, Sexo sexo, Conta conta) {
		this.nome = nome;
		this.sexo = sexo;
		this.conta = conta;
		validar();
	}

	public Pessoa(String nome, String sobrenome, Calendar nascimento,
			Sexo sexo, Conta conta) {
		this.nome = nome;
		this.sobrenome = sobrenome;
		this.nascimento = nascimento;
		this.sexo = sexo;
		this.conta = conta;
		validar();
	}

	public Pessoa(long id, String nome, String sobrenome, Calendar nascimento,
			Sexo sexo, Conta conta) {
		this.id = id;
		this.nome = nome;
		this.sobrenome = sobrenome;
		this.nascimento = nascimento;
		this.sexo = sexo;
		this.conta = conta;
		validar();
	}

	private void validar() {
		verificaNuloOuVazio(this.nome, "Nome não pode ser nulo.");
		verificaNuloOuVazio(this.sexo, "Sexo não pode ser nulo.");
		verificaNuloOuVazio(this.conta, "Conta não pode ser nula.");
	}

	public Pessoa(String nome) {
		this.nome = nome;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public Calendar getNascimento() {
		return nascimento;
	}

	public void setNascimento(Calendar nascimento) {
		this.nascimento = nascimento;
	}

	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	public Conta getConta() {
		return conta;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

}
