package br.flpcostas.projeto.dominio.modelo.objetovalor;

public enum Sexo {
	
	MASCULINO(1), FEMININO(2);
	public int valor;

	Sexo(int valor) {
		this.valor = valor;
	}

}
