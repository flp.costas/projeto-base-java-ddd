package br.flpcostas.projeto.dominio.modelo.entidade;

import java.util.Calendar;

public class Modelo extends EntidadeBase {

	private long id;
	private String campoTexto;
	private Calendar campoData;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCampoTexto() {
		return campoTexto;
	}

	public void setCampoTexto(String campoTexto) {
		this.campoTexto = campoTexto;
	}

	public Calendar getCampoData() {
		return campoData;
	}

	public void setCampoData(Calendar campoData) {
		this.campoData = campoData;
	}

}
