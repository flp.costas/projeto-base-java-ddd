package br.flpcostas.projeto.dominio.repositorio;

public interface SeedRepositorio {

	void seed();
	
}
