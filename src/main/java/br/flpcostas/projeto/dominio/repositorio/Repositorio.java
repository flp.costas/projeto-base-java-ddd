package br.flpcostas.projeto.dominio.repositorio;

import java.util.Collection;

public interface Repositorio<T> {

	T porId(long id);

	Collection<T> listarTodos();

	T salvar(T o);
}
