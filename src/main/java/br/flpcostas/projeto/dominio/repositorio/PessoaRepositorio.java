package br.flpcostas.projeto.dominio.repositorio;

import java.util.Collection;

import br.flpcostas.projeto.dominio.modelo.entidade.Pessoa;

public interface PessoaRepositorio {

	Collection<Pessoa> porNome(String nome);

	Pessoa porEmail(String email);

	Pessoa porLogin(String login);
	
	Pessoa salvar(Pessoa pessoa);

}
