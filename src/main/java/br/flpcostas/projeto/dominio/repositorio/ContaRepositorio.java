package br.flpcostas.projeto.dominio.repositorio;

import br.flpcostas.projeto.dominio.modelo.entidade.Conta;

public interface ContaRepositorio {
	Conta porLoginESenha(String lg, String sn);
}
