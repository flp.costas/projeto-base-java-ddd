package br.flpcostas.projeto.apresentacao.servico;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "servico-exception")
public class ServicoException {

	private String mensagem;
	private int status;

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public ServicoException() {
	}

	public ServicoException(String mensagem) {
		this.mensagem = mensagem;
	}
	
	public ServicoException(String mensagem, int status) {
		this.mensagem = mensagem;
		this.status = status;
	}
}
