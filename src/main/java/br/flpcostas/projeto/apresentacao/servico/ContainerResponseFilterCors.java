package br.flpcostas.projeto.apresentacao.servico;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerResponse;
import com.sun.jersey.spi.container.ContainerResponseFilter;

public class ContainerResponseFilterCors implements ContainerResponseFilter {

	@Override
	public ContainerResponse filter(ContainerRequest req, ContainerResponse resp) {

		String metodo = req.getHeaderValue("Access-Control-Request-Method");
		if (metodo != null)
			req.setMethod(metodo);

		ResponseBuilder resposta = Response.fromResponse(resp.getResponse());
		resposta.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Request-Headers",
						"Origin, X-Requested-With, Content-Type, Accept, Authorization")
				.header("Access-Control-Allow-Methods",
						"GET, POST, PUT, DELETE, OPTIONS");

		String reqHeaders = req.getHeaderValue("Access-Control-Request-Headers");

		if (null != reqHeaders && !reqHeaders.equals("")) {
			resposta.header("Access-Control-Allow-Headers", reqHeaders);
		}

		resp.setResponse(resposta.build());

		return resp;
	}

}
