package br.flpcostas.projeto.apresentacao.servico.dto;

import br.flpcostas.projeto.dominio.modelo.entidade.Conta;
import br.flpcostas.projeto.dominio.modelo.entidade.Pessoa;
import br.flpcostas.projeto.dominio.modelo.objetovalor.Perfil;
import br.flpcostas.projeto.dominio.modelo.objetovalor.Sexo;

public class ContaDtoConverte {

	public static ContaDto toDto(Pessoa entidade) {

		ContaDto dto = new ContaDto(entidade.getId(), entidade.getNome(),
				entidade.getSobrenome(), entidade.getSexo().toString(),
				entidade.getConta().getLogin(), entidade.getConta().getEmail(),
				entidade.getConta().getSenha());

		return dto;
	}

	public static Pessoa fromDto(ContaDto dto) {

		Pessoa entidade = new Pessoa(dto.getNome(),
				dto.getSobrenome(), null, Sexo.valueOf(dto.getSexo()),
				new Conta(dto.getLogin(), dto.getEmail(), dto.getSenha(),
						Perfil.USUARIO));

		return entidade;
	}
}
