package br.flpcostas.projeto.apresentacao.servico;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.flpcostas.projeto.aplicacao.AutenticacaoServico;
import br.flpcostas.projeto.aplicacao.ContaServico;
import br.flpcostas.projeto.apresentacao.servico.dto.ContaDto;
import br.flpcostas.projeto.apresentacao.servico.dto.ContaDtoConverte;
import br.flpcostas.projeto.dominio.modelo.entidade.Pessoa;
import br.flpcostas.projeto.infraestrutura.injecaodep.guice.GuiceModuloRegistro;

import com.google.inject.Guice;
import com.google.inject.Injector;

/**
 * 
 * @author filipe
 *
 *         Classe base de servico rest com jersey
 * 
 *         Orientacoes base para consumo do servico:
 * 
 * @Produces: Indica o tipo de recurso que o servico ira entregar Cliente deve
 *            utilizar Accept para escolher o tipo mime que deseja receber (ex:
 *            Accept:application/xml)
 * 
 * @Consumes: Indica o tipo de recurso o cliente precisara consumir. Cliente
 *            deve utilizar Content-Type para informar o formato de recurso que
 *            ira enviar, caso ao contrario, o servico nao respondera
 *            corretamente (ex: Content-Type:application/json)
 *
 */
@Path("/v1/servico/conta")
public class ContaServicoFachada {

	@Context
	HttpHeaders headers;
	private ContaServico contaServico;
	private AutenticacaoServico autenticacaoServico;

	public ContaServicoFachada() {
		Injector injector = Guice.createInjector(new GuiceModuloRegistro());
		contaServico = injector.getInstance(ContaServico.class);
		autenticacaoServico = injector.getInstance(AutenticacaoServico.class);
	}

	/**
	 * @api {post} /servico/conta/login Efetua Login
	 * @apiName Login
	 * @apiGroup Autenticacao
	 *
	 *
	 * @apiHeaderExample {json} Header-Example: { "Token":
	 *                   "sw0923lcxkc1239glti321" }
	 *
	 *
	 * @apiParamExample {json} Request-Example: { "login": "flpcostas", "senha":
	 *                  "A665A45920422F9D417E4867EFDC4FB8A04A1F3FFF1FA07E998E86F7F7A27AE3"
	 *                  }
	 *
	 * 
	 * @apiSuccess {json} Objeto usuario em Json ou Xml
	 * @apiSuccessExample Success-Response: HTTP/1.1 200 OK { "id":1,
	 *                    "nome":"Filipe", "sobrenome":"Costa", "conta": {
	 *                    "email": "flp.costas@gmail.com", "login": "flpcostas",
	 *                    "senha":
	 *                    "A665A45920422F9D417E4867EFDC4FB8A04A1F3FFF1FA07E998E86F7F7A27AE3"
	 *                    , "perfil": "USUARIO" } }
	 *
	 *
	 * @apiError UserNotFound Usuario nao encontrado
	 * @apiErrorExample Error-Response: HTTP/1.1 404 Not Found { "mensagem":
	 *                  "Houve um erro na requisicao" }
	 */
	@POST
	@Path("/login")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_FORM_URLENCODED })
	public Response login(@FormParam("login") String login,
			@FormParam("senha") String senha) {
		try {
			Pessoa pessoa = autenticacaoServico.efetuarLogin(login, senha);
			if (pessoa == null) {
				throw new WebApplicationException(Response
						.status(Status.NOT_FOUND)
						.entity(new ServicoException(
								"Login ou senha inválidos!", 404)).build());
			}
			ContaDto dto = ContaDtoConverte.toDto(pessoa);
			Response response = Response
					.status(Status.OK)
					.header("Token-Acesso",
							autenticacaoServico.getTokenAcesso()).entity(dto)
					.build();

			return response;

		} catch (WebApplicationException ex) {
			return ex.getResponse();
		} catch (Exception ex) {
			return Response.status(Status.BAD_REQUEST)
					.entity(new ServicoException(ex.getMessage(), 400)).build();
		}
	}

	
	/**
	 * @api {post} /servico/conta/criar Cadastrar Conta
	 * @apiName Cadastrar
	 * @apiGroup Conta
	 *
	 *
	 * @apiHeaderExample {json} Header-Example: { "Token":
	 *                   "sw0923lcxkc1239glti321" }
	 *
	 *
	 * @apiParamExample {json} Request-Example: { "login": "flpcostas", "senha":
	 *                  "A665A45920422F9D417E4867EFDC4FB8A04A1F3FFF1FA07E998E86F7F7A27AE3"
	 *                  }
	 *
	 * 
	 * @apiSuccess {json} Objeto usuario em Json ou Xml
	 * @apiSuccessExample Success-Response: HTTP/1.1 200 OK { "id":1,
	 *                    "nome":"Filipe", "sobrenome":"Costa", "conta": {
	 *                    "email": "flp.costas@gmail.com", "login": "flpcostas",
	 *                    "senha":
	 *                    "A665A45920422F9D417E4867EFDC4FB8A04A1F3FFF1FA07E998E86F7F7A27AE3"
	 *                    , "perfil": "USUARIO" } }
	 *
	 *
	 * @apiError UserNotFound Usuario nao encontrado
	 * @apiErrorExample Error-Response: HTTP/1.1 404 Not Found { "mensagem":
	 *                  "Houve um erro na requisicao" }
	 */
	@POST
	@Path("/criar")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Response criar(ContaDto conta) {
		try {
			Pessoa pessoa = ContaDtoConverte.fromDto(conta);
			Pessoa pessoaSalva = contaServico.criar(pessoa);
			Response response = Response.status(Status.OK)
					.entity(ContaDtoConverte.toDto(pessoaSalva)).build();

			return response;

		} catch (Exception ex) {
			return Response.status(Status.BAD_REQUEST)
					.entity(new ServicoException(ex.getMessage(), 400)).build();
		}
	}

}
