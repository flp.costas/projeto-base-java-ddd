package br.flpcostas.projeto.apresentacao.servico;

import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.flpcostas.projeto.apresentacao.servico.dto.ContaDto;
import br.flpcostas.projeto.dominio.repositorio.SeedRepositorio;
import br.flpcostas.projeto.infraestrutura.persistencia.jpa.SeedRepositorioJpa;

/**
 * 
 * @author filipe
 *
 *         Classe base de servico rest com jersey
 * 
 *         Orientacoes base para consumo do servico:
 * 
 * @Produces: Indica o tipo de recurso que o servico ira entregar Cliente deve
 *            utilizar Accept para escolher o tipo mime que deseja receber (ex:
 *            Accept:application/xml)
 * 
 * @Consumes: Indica o tipo de recurso o cliente precisara consumir. Cliente
 *            deve utilizar Content-Type para informar o formato de recurso que
 *            ira enviar, caso ao contrario, o servico nao respondera
 *            corretamente (ex: Content-Type:application/json)
 *
 */
@Path("/v1/servico")
public class ServicoFachada {

	@Context
	HttpHeaders headers;
	private SeedRepositorio repositorio;

	public ServicoFachada() {
		// Injector injector = Guice.createInjector(new GuiceModuloRegistro());
		repositorio = new SeedRepositorioJpa();
	}

	@GET
	@Path("/get")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Response produce() {
		ContaDto c = new ContaDto();
		c.setNome("Filipe");
		c.setSobrenome("Costa");
		return Response.status(200).entity(c).build();
	}

	@POST
	@Path("/post")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Response consume(ContaDto c) {
		ContaDto c1 = new ContaDto();
		c1.setNome("Filipe");
		c1.setSobrenome("Costa");
		return Response.status(200).entity(c).build();
	}

	@OPTIONS
	@Path("/opt")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Response opt() {
		ContaDto c = new ContaDto();
		c.setNome("Filipe");
		c.setSobrenome("Costa");
		return Response.status(200).entity(c).build();
	}

	@GET
	@Path("/versao")
	public Response getVersao() {
		return Response.status(Status.OK).entity(new String("1.0-SNAPSHOT"))
				.build();
	}

	@GET
	@Path("/seed")
	public Response seed() {
		repositorio.seed();
		return Response.status(Status.CREATED)
				.entity(new String("Seed executada com sucesso!")).build();
	}

}
