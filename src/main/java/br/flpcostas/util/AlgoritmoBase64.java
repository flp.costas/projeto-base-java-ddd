package br.flpcostas.util;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

public class AlgoritmoBase64 {

	public static String encode(String str) {
		return Base64.getEncoder().encodeToString(str.getBytes());

	}

	public static String decode(String str) {
		byte[] decode = Base64.getDecoder().decode(str.getBytes());
		try {
			return new String(decode, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			
		}
		return null;
	}

}
