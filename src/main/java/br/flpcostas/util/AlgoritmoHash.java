package br.flpcostas.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class AlgoritmoHash {

	public static String ALGORITMO = "SHA-256";

	public static String getHash(String termo) {
		MessageDigest algorithm = null;
		StringBuilder hexString = new StringBuilder();
		try {
			algorithm = MessageDigest.getInstance(ALGORITMO);
			byte messageDigest[];
			messageDigest = algorithm.digest(termo.getBytes("UTF-8"));

			for (byte b : messageDigest) {
				hexString.append(String.format("%02X", 0xFF & b));
			}
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {

		}

		return hexString.toString();

	}

}
