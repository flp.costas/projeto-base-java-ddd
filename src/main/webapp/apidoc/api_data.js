define({ "api": [
  {
    "type": "post",
    "url": "/servico/conta/login",
    "title": "Efetua Login",
    "name": "Login",
    "group": "Autenticacao",
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{ \"Token\": \"sw0923lcxkc1239glti321\" }",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "examples": [
        {
          "title": "Request-Example: ",
          "content": "{ \"login\": \"flpcostas\", \"senha\": \"A665A45920422F9D417E4867EFDC4FB8A04A1F3FFF1FA07E998E86F7F7A27AE3\" }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "Objeto",
            "description": "<p>usuario em Json ou Xml</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response: HTTP/1.1 200 OK ",
          "content": "{ \"id\":1, \"nome\":\"Filipe\", \"sobrenome\":\"Costa\", \"conta\": { \"email\": \"flp.costas@gmail.com\", \"login\": \"flpcostas\", \"senha\": \"A665A45920422F9D417E4867EFDC4FB8A04A1F3FFF1FA07E998E86F7F7A27AE3\", \"perfil\": \"USUARIO\" } }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserNotFound",
            "description": "<p>Usuario nao encontrado</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response: HTTP/1.1 404 Not Found ",
          "content": "{ \"mensagem\": \"Houve um erro na requisicao\" }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./Servico.java",
    "groupTitle": "Autenticacao"
  },
  {
    "type": "get",
    "url": "/servico/versao",
    "title": "Retorna versao",
    "name": "Versao",
    "group": "Servico",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Objeto",
            "description": "<p>containter de requisicao.</p> "
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./Servico.java",
    "groupTitle": "Servico"
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p> "
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p> "
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./apidoc-v1/main.js",
    "group": "_media_filipe_DADOS_PROJETOS_API_DOC_bib_acesso_apidoc_v1_main_js",
    "groupTitle": "_media_filipe_DADOS_PROJETOS_API_DOC_bib_acesso_apidoc_v1_main_js",
    "name": ""
  }
] });
